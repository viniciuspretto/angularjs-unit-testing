class TestingHelper {
  constructor ($rootScope, $compile) {
    this.element;
    this.$rootScope = $rootScope;
    this.$compile = $compile;
  }

  render (component, bindings) {
    const scope = this.$rootScope.$new();

    if (bindings && Object.keys(bindings).length > 0) {
      Object.keys(bindings).forEach(key => {
        scope[key] = bindings[key];
      });
    }

    const element = angular.element(component);
    this.element = this.$compile(element)(scope);
    scope.$digest();
  }

  get (selector) {
    return this.element[0].querySelector(selector);
  }

  getText (selector) {
    return this.get(selector).textContent.trim();
  }

  hasClass (selector, className) {
    return this.getAngularElement(selector).hasClass(className);
  }

  isHidden (selector) {
    return this.hasClass(selector, 'ng-hide');
  }

  contains (selector) {
    return this.element[0].contains(this.get(selector));
  }

  type (selector, value) {
    this.getAngularElement(selector)
      .val(value)
      .triggerHandler('change');
  }

  click (selector) {
    this.get(selector).click();
  }

  submit (selector) {
    this.getAngularElement(selector).triggerHandler('submit');
  }

  getAngularElement (selector) {
    return angular.element(this.get(selector));
  }
}
