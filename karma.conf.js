module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['mocha', 'chai'],
    files: [
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      { pattern: './test/**/*.js' },
      { pattern: './src/**/*.js' },
      { pattern: './src/**/*.tmpl.html' }
    ],
    exclude: [],
    preprocessors: {
      'src/**/*.tmpl.html': ['ng-html2js'],
      'src/**/!(*spec).js': ['coverage']
    },
    ngHtml2JsPreprocessor: {
      stripPrefix: 'src/',
      moduleName: 'templates'
    },
    coverageReporter: {
      type: 'html',
      dir: 'coverage/',
      subdir: 'report'
    },
    reporters: ['mocha', 'coverage'],
    mochaReporter: {
      colors: {
        success: 'greenBright',
        error: 'bgRedBright'
      },
      divider: '.',
      showDiff: true
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_WARN,
    autoWatch: true,
    browsers: ['ChromeHeadlessNoSandbox'],
    singleRun: false,
    concurrency: Infinity,
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox', '--disable-setuid-sandbox']
      },
      ChromeDebugging: {
        base: 'Chrome',
        flags: ['--remote-debugging-port=9333']
      }
    }
  });
};
