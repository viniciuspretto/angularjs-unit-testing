# AngularJS Unit Testing

Projeto têm objetivo de apresentar como realizar testes de unidade no AngularJS.

## Requisitos

- Node.js >= 10.24

## Referências

- [AngularJS Unit Testing Docs](https://docs.angularjs.org/guide/unit-testing)
- [Karma](https://karma-runner.github.io/latest/index.html)

## Iniciar a aplicação

**1. Instalar as dependências**

```
$ npm install
```

**2. Executar a aplicação**

```
$ npm start
```

## Testes

```
$ npm test
```

## Coverage

```
$ npm run test:coverage
```
