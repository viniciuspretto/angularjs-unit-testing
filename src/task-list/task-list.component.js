(function () {
  'use strict';

  angular.module('app').component('taskList', {
    templateUrl: 'task-list/task-list.tmpl.html',
    controller: controller,
    controllerAs: 'vm',
    bindings: {}
  });

  function controller ($scope) {
    var vm = this;

    vm.tasks = [];
    vm.onAddTask = onAddTask;
    vm.onDeleteTask = onDeleteTask;

    function onAddTask () {
      if ($scope.taskListForm.$invalid) {
        return;
      }
      var task = {
        id: Date.now(),
        name: vm.taskName,
        checked: false
      };
      vm.tasks.push(task);
      vm.taskName = '';
      $scope.taskListForm.$setPristine();
    }

    function onDeleteTask (taskId) {
      var index = vm.tasks.findIndex(function (task) {
        return task.id === taskId;
      });
      vm.tasks.splice(index, 1);
    }
  }
})();
