describe('Task List component', () => {
  let testingHelper;
  const FieldSelector = {
    TITLE: 'h2',
    INPUT: '#task-name',
    BUTTON: '#add-task-btn',
    FORM: '#task-list-form',
    LIST_ITEM_CHECKBOX: '#task-list li input',
    LIST_ITEM_LABEL: '#task-list li label',
    LIST_ITEM_DELETE_BUTTON: '#task-list li button',
    TASK_LIST: '#task-list',
    ERROR_MESSAGE: '#error-message'
  };

  beforeEach(() => {
    module('app');
    module('templates');
    inject(($rootScope, $compile) => {
      testingHelper = new TestingHelper($rootScope, $compile);
    });

    testingHelper.render('<task-list />');
    testingHelper.type(FieldSelector.INPUT, 'Comprar vinho');
    testingHelper.submit(FieldSelector.FORM);
  });

  it('should not be able submit empty task', () => {
    const taskList = testingHelper.get(FieldSelector.TASK_LIST).children;
    expect(taskList.length).to.be.eql(1);

    testingHelper.type(FieldSelector.INPUT, 'Criar o teste');
    testingHelper.type(FieldSelector.INPUT, '');
    testingHelper.submit(FieldSelector.FORM);

    const button = testingHelper.get(FieldSelector.BUTTON);
    const errorMessageIsHidden = testingHelper.isHidden(
      FieldSelector.ERROR_MESSAGE
    );
    const errorMessage = testingHelper.getText(FieldSelector.ERROR_MESSAGE);

    expect(button.disabled).to.be.eql(true);
    expect(errorMessageIsHidden).to.be.eql(false);
    expect(errorMessage).to.be.eql('Favor preencher o campo');
    expect(taskList.length).to.be.eql(1);
  });

  it('should display task after form submit', () => {
    const title = testingHelper.getText(FieldSelector.TITLE);
    const taskList = testingHelper.get(FieldSelector.TASK_LIST).children;
    const inputCheckbox = testingHelper.get(FieldSelector.LIST_ITEM_CHECKBOX);
    const label = testingHelper.getText(FieldSelector.LIST_ITEM_LABEL);

    expect(title).to.be.eql('Lista de tarefas');
    expect(taskList.length).to.be.eql(1);
    expect(inputCheckbox.checked).to.be.eql(false);
    expect(label).to.be.eql('Comprar vinho');
  });

  it('should have class "checked" when task is checked', () => {
    testingHelper.click(FieldSelector.LIST_ITEM_CHECKBOX);

    const inputCheckbox = testingHelper.get(FieldSelector.LIST_ITEM_CHECKBOX);
    const labelContainsCheckedClass = testingHelper.hasClass(
      FieldSelector.LIST_ITEM_LABEL,
      'checked'
    );

    expect(inputCheckbox.checked).to.be.eql(true);
    expect(labelContainsCheckedClass).to.be.eql(true);
  });

  it('should remove task when clicks on "delete" button', () => {
    testingHelper.click(FieldSelector.LIST_ITEM_DELETE_BUTTON);

    const titleIsHidden = testingHelper.isHidden(FieldSelector.TITLE);
    const taskList = testingHelper.get(FieldSelector.TASK_LIST).children;

    expect(titleIsHidden).to.be.eql(true);
    expect(taskList.length).to.be.eql(0);
  });
});
